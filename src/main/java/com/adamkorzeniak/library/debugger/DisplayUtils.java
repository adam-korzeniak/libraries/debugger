package com.adamkorzeniak.library.debugger;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

class DisplayUtils {

    private static final String SPACE = " ";

    private DisplayUtils() {}

    public static int getLength(Collection<String> values) {
        return values.stream()
                .mapToInt(String::length)
                .max()
                .orElse(0);
    }

    public static String toLength(String text, int length) {
        int textLength = text.length();
        if (length <= textLength) {
            return text;
        }
        return text + SPACE.repeat(length - textLength);
    }

    public static DecimalFormat getDoubleFormatter(Collection<Double> values) {
        int intDigits = values.stream()
                .map(Double::intValue)
                .mapToInt(val -> val.toString().length())
                .max()
                .orElse(0);
        int decimalDigits = values.stream()
                .map(val -> val.toString().replaceAll("^\\d+\\.", ""))
                .mapToInt(val -> val.replaceAll("0+$", "").length())
                .max()
                .orElse(0);
        String format = String.format("%s.%s", "0".repeat(intDigits), "0".repeat(decimalDigits));
        return new DecimalFormat(format);
    }

    public static List<DecimalFormat> getDoubleFormatters(List<? extends Collection<Double>> values) {
        return values.stream()
                .map(DisplayUtils::getDoubleFormatter)
                .collect(Collectors.toList());
    }
}
