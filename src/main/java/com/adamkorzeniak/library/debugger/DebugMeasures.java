package com.adamkorzeniak.library.debugger;

import java.text.DecimalFormat;
import java.util.LinkedHashMap;

class DebugMeasures {
    public LinkedHashMap<String, Double> measures = new LinkedHashMap<>();

    public void inc(String key) {
        add(key, 1d);
    }

    public void add(String key, double value) {
        if (measures.containsKey(key)) {
            measures.put(key, measures.get(key) + value);
        } else {
            measures.put(key, value);
        }
    }

    public String getPrettyContent() {
        int keyLength = DisplayUtils.getLength(measures.keySet());
        DecimalFormat valueFormatter = DisplayUtils.getDoubleFormatter(measures.values());
        StringBuilder sb = new StringBuilder("Values: \n");
        for (var entry: measures.entrySet()) {
            String key = DisplayUtils.toLength(entry.getKey(), keyLength);
            String value = valueFormatter.format(entry.getValue());
            sb.append(String.format("%s : %s%n", key, value));
        }
        return sb.toString();
    }
}
