package com.adamkorzeniak.library.debugger;

public class Debugger {

    private final DebugMeasures debug = new DebugMeasures();
    private final PerformanceMeasures performance = new PerformanceMeasures();

    public void inc(String key) {
        debug.inc(key);
    }

    public void add(String key, double value) {
        debug.add(key, value);
    }

    public void start(String key) {
        performance.start(key);
    }

    public void stop(String key) {
        performance.stop(key);
    }

    public void print() {
        System.out.printf("%s%n%s%n", debug.getPrettyContent(),performance.getPrettyContent());
    }


}
