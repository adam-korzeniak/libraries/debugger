package com.adamkorzeniak.library.debugger;

import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

class PerformanceMeasures {

    private final LinkedHashMap<String, PerformanceItem> measures = new LinkedHashMap<>();

    public void start(String key) {
        getItem(key).start();
    }

    public void stop(String key) {
        getItem(key).stop();
    }

    private PerformanceItem getItem(String key) {
        if (measures.containsKey(key)) {
            return measures.get(key);
        }
        PerformanceItem item = new PerformanceItem(key);
        measures.put(key, item);
        return item;
    }

    public String getPrettyContent() {
        int keyLength = DisplayUtils.getLength(measures.keySet());
        var values = measures.values().stream()
                .map(PerformanceItem::getValues)
                .collect(Collectors.toList());
        List<DecimalFormat> valueFormatters = DisplayUtils.getDoubleFormatters(values);
        StringBuilder sb = new StringBuilder("Performance: \n");
        for (var value: measures.values()) {
            sb.append(value.getPrettyContent(keyLength, valueFormatters));
        }
        return sb.toString();
    }
}
