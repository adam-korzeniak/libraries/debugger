package com.adamkorzeniak.library.debugger.exception;

public class TaskMeasurementException extends RuntimeException {
    private final String taskKey;

    public TaskMeasurementException(String key) {
        this.taskKey = key;
    }
}
