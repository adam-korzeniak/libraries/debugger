package com.adamkorzeniak.library.debugger.exception;

public class TaskNotStartedException extends TaskMeasurementException {
    public TaskNotStartedException(String key) {
        super(key);
    }
}
