package com.adamkorzeniak.library.debugger.exception;

public class TaskAlreadyFinishedException extends TaskMeasurementException {
    public TaskAlreadyFinishedException(String key) {
        super(key);
    }
}
