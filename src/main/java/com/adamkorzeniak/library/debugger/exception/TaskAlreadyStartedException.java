package com.adamkorzeniak.library.debugger.exception;

public class TaskAlreadyStartedException extends TaskMeasurementException {
    public TaskAlreadyStartedException(String key) {
        super(key);
    }
}
