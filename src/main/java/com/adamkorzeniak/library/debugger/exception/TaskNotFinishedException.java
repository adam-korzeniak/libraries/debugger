package com.adamkorzeniak.library.debugger.exception;

public class TaskNotFinishedException extends TaskMeasurementException {
    public TaskNotFinishedException(String key) {
        super(key);
    }
}
