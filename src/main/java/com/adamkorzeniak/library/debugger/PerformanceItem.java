package com.adamkorzeniak.library.debugger;

import com.adamkorzeniak.library.debugger.exception.TaskAlreadyFinishedException;
import com.adamkorzeniak.library.debugger.exception.TaskAlreadyStartedException;
import com.adamkorzeniak.library.debugger.exception.TaskNotFinishedException;
import com.adamkorzeniak.library.debugger.exception.TaskNotStartedException;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class PerformanceItem {
    private String key;
    private long start;
    private long end;
    private long duration;

    public PerformanceItem(String key) {
        this.key = key;
    }

    public void start() {
        if (start > 0) {
            throw new TaskAlreadyStartedException(key);
        }
        start = System.currentTimeMillis();
    }

    public void stop() {
        if (start == 0) {
            throw new TaskNotStartedException(key);
        }
        if (end > 0) {
            throw new TaskAlreadyFinishedException(key);
        }
        end = System.currentTimeMillis();
        duration = end - start;
    }

    public long getDuration() {
        if (start == 0) {
            throw new TaskNotStartedException(key);
        }
        if (end == 0) {
            throw new TaskNotFinishedException(key);
        }
        return duration;
    }

    public List<String> getHeadings() {
        return new ArrayList<>();
    }

    public List<Double> getValues() {
        return new ArrayList<>();
    }

    public String getPrettyContent(int keyLength, Collection<DecimalFormat> decimalFormats) {
        String keyText = DisplayUtils.toLength(key, keyLength);
        StringBuilder sb = new StringBuilder(keyText);
        sb.append(" : ");
        for (var value: getValues()) {
            sb.append()
            String value = valueFormatter.format(entry.getValue());
            sb.append(String.format("%s : %s%n", key, value));
        }
        return sb.toString();
    }
}
