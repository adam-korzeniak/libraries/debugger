package com.adamkorzeniak.library.debugger.example;

import com.adamkorzeniak.library.debugger.Debugger;

public class ExampleClient {
    public static void main(String[] args) {
        Debugger debugger = new Debugger();
        debugger.inc("count");
        debugger.inc("count");
        debugger.add("total big big long", 4.43);
        debugger.inc("count");
        debugger.add("total", 9.4);
        debugger.inc("total");

        debugger.print();
    }
}
